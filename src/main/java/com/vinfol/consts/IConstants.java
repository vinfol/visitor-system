package com.vinfol.consts;

public interface IConstants
{

  public static final String RESPONSE_STATUS_CODE_FAILED = "201";
  public static final String RESPONSE_STATUS_CODE_PART_FAILED = "202";
  public static final String RESPONSE_STATUS_CODE_NOT_FOUND = "404";
  public static final String RESPONSE_STATUS_CODE_SUCCESS = "200";
  public static final String RESPONSE_STATUS_CODE_SUCCESS_MSG = "success";
  
  public static final String USERMODELMSG = "USERMODELMSG";
  

}
