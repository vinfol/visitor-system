package com.vinfol.web;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class NeoController {
	private static final Logger log = LoggerFactory.getLogger(NeoController.class);

	private final RestTemplate restTemplate;

	@Autowired
	public NeoController(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public String getHello() {
		// 建立连接
		log.info("getData接口调用开始时间:" + new Date());// 接口调用开始时间

		final String plainCreds = "neo4j:neo4j!@1234";
		final byte[] plainCredsBytes = plainCreds.getBytes();
		final byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		final String base64Creds = new String(base64CredsBytes);
		String auth = "Basic " + base64Creds;
		System.out.println("auth:  " + auth);

		final HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", auth);
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(
				Arrays.asList(new MediaType[] { new MediaType("application", "json", Charset.forName("UTF-8")) }));
		final HttpEntity<String> request = new HttpEntity<>("{\n" + "  \"statements\" : [ {\n"
				+ "    \"statement\" : \"MATCH (n) RETURN n LIMIT 25\"\n" + "  } ]\n" + "}", headers);

		final ResponseEntity<String> response = restTemplate.exchange(
				"http://localhost:7474/db/data/transaction/commit", HttpMethod.POST, request, String.class);
		final String respString = response.getBody();

		return respString;
	}
}