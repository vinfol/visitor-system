package com.vinfol.config.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 登录配置 博客出处：http://www.cnblogs.com/GoodHelper/
 *
 */
@Configuration
public class WebSecurityConfig extends WebMvcConfigurerAdapter {
	private Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);
	@Value("${token.header}")
	private String tokenHeader;

	@Value("${token.name}")
	private String tokenName;

	// 跳转登录
	private static final String LOGIN_URL = "/login";

	/**
	 * 登录session key
	 */
	public final static String SESSION_KEY = "user";

	@Bean
	public SecurityInterceptor getSecurityInterceptor() {
		return new SecurityInterceptor();
	}

	public void addInterceptors(InterceptorRegistry registry) {
		logger.info("addInterceptors");
		InterceptorRegistration addInterceptor = registry.addInterceptor(getSecurityInterceptor());

		// 排除配置
		addInterceptor.excludePathPatterns("/error");
		addInterceptor.excludePathPatterns("/login**");

		// 拦截配置
		addInterceptor.addPathPatterns("/**");
	}

	private class SecurityInterceptor extends HandlerInterceptorAdapter {

		@Override
		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
				throws Exception {
			logger.info("SecurityInterceptor preHandle");
			if (authLogin(request, response)) {
				return true;
			}
			response.sendRedirect(LOGIN_URL);
			return false;
		}
	}

	public boolean authLogin(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		logger.info("attemptAuthentication");
		String authToken = request.getHeader(this.tokenHeader);
		if (StringUtils.isEmpty(authToken)) {
			authToken = request.getParameter(this.tokenName);
		}
		logger.debug("start to check token:{} *************");
//		if (authToken == null) {
////			return false;
//			throw new RuntimeException("Access Token is not provided");
//		}
		return true;

	}

}
